import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int answer = 1;
        int counter = 1;
        int factorial = 1;

        try {
            System.out.println("Input an integer to calculate its factorial");
            answer = in.nextInt();

        } catch (Exception e){
            System.out.println("Invalid input. Enter an Integer");
            System.exit(0);
        }

        if (answer <= 0) {
            System.out.println("Factorial is undefined for 0 and negative numbers.");
        } else {

            while (counter <= answer) {
                factorial *= counter;
                counter++;
            }

            System.out.println("Factorial of " + answer + " is " + factorial);
            factorial = 1;
        }
        try {
            System.out.println("Input an integer to calculate its factorial");
            answer = in.nextInt();

        } catch (Exception e) {
            System.out.println("Invalid input. Enter an Integer");
            System.exit(0);
        }

        if (answer <= 0) {
            System.out.println("Factorial is not defined for 0 and negative numbers.");
        } else {
            for (int i = 1; i <= answer; i++) {
                factorial *= i;
            }
            System.out.println("Factorial of " + answer + " is " + factorial);
        }


    }
}